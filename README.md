# Présentation Docker

Sources `markdown` pour ma formation Docker donnée en 2017.

## Description

Le contenu principal est dans le fichier [content.md](/content.md).
Les images sont dans le répertoire [/img](/img).

## Image Docker

Une image Docker est produite au merge dans `master` avec la présentation.
Celle-ci peut se lancer avec la commande suivante :

```bash
docker run -it --rm -p 8000:8000 registry.gitlab.com/kalmac/formation-docker:latest
```

## Développement

Pour le dev, il est possible d'utiliser l'image Docker afin de voir en live
ses modifications.

```bash
docker run -it --rm -p 8000:8000 \
  -v $PWD/index.html:/reveal.js/index.html \
  -v $PWD/content.md:/reveal.js/content.md \
  -v $PWD/img:/reveal.js/img \
  -v $PWD/css/custom.css:/reveal.js/css/custom.css \
  registry.gitlab.com/kalmac/formation-docker
```
